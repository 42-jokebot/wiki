# JokeBot

### [Развертывание приложения](./services-installation.md)

## Описание

Телеграм бот для шуток. Шутки получаются с различных веб-ресурсов по категориям и языкам (ru и en). Приложение собирает
статистику по понравившимся шуткам и содержит информацию о пользователях. Статистика собирается как по пользователям,
так и по шуткам и категориям. Приложение поддерживает журналирование действий пользователей и кеширование шуток и категорий.

## Архитектура

Приложение построено на микросервисной архитектуре:
- [сервис интеграции с веб-ресурсами](https://gitlab.com/42-jokebot/integration-service)
- [сервис по работе с шутками](https://gitlab.com/42-jokebot/jokes-service)
- [сервис по работе со статистикой](https://gitlab.com/42-jokebot/statistics-service)

<img src="./images/system-architecture.png" alt="Архитектура системы" width="500"/>

*Дополнительно написаны:*
- *[стартер для логирования](https://gitlab.com/42-jokebot/logging-spring-boot-starter)*
- *[стартер для валидации (enum и timerange)](https://gitlab.com/42-jokebot/validation-spring-boot-starter)*

## Стек технологий

- Java 21
- Spring Boot 3
- MyBatis
- Apache Kafka
- Gradle
- Docker
- JUnit 5
- PostgreSQL
- Victoria Metrics
- Zipkin