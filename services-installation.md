# JokeBot

## Профили Spring

- `[default]` - для запуска сервисов `jokes-service`, `integration-service` и `stats-service` локально без Docker.
- `container` - для развертывания сервисов `jokes-service`, `integration-service` и `stats-service` c помощью Docker Сompose (примеры готовой конфигурации docker-compose.yml, environment variables и конфигов находятся в директории ([containerize](./containerize)).

## Инфраструктура

### PostgreSQL

В сервисах `jokes-service`, `integration-service` и `stats-service` используется в качестве БД.

Запуск в Docker:

```shell
docker run --name db-joke -p 5433:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=jokes postgres:16
```
```shell
docker run --name db-stats -p 5434:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=stats postgres:16
```
```shell
docker run --name db-integration -p 5435:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=integration postgres:16
```

### Apache Kafka

В архитектуре используется в качестве брокера сообщений для асинхронного обновления статистики.

Запуск в Docker:

Для работы Kafka необходим сервис ZooKeeper в той же сети:
```shell
docker network create kafka-net
```
```shell
docker run --name zookeeper --network kafka-net -p 2181:2181 -e ZOOKEEPER_CLIENT_PORT=2181 confluentinc/cp-zookeeper:7.6.1
```
```shell
docker run --name kafka --network kafka-net -p 9092:9092 -e KAFKA_LISTENER_SECURITY_PROTOCOL_MAP=PLAINTEXT:PLAINTEXT,PLAINTEXT_HOST:PLAINTEXT -e KAFKA_ZOOKEEPER_CONNECT=zookeeper:2181 -e KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://kafka:29092,PLAINTEXT_HOST://localhost:9092 -e KAFKA_BROKER_ID=1 confluentinc/cp-kafka:7.6.1
```

### Victoria Metrics

В проекте используется для сбора метрик сервисов ([пример promscape.yml](./config/victoria-metrics/promscrape.yml)).

Запуск в Docker:

```shell
docker run --name victoria-metrics -p 8428:8428 -v ./config/victoria-metrics/promscrape.yml:/etc/victoria-metrics/promscrape.yml victoriametrics/victoria-metrics:v1.101.0 -promscrape.config=/etc/victoria-metrics/promscrape.yml
```

### Grafana

В проекте используется для визуализации метрик и логов.

Запуск в Docker:

```shell
docker run --name grafana -p 3000:3000 -v ./data/grafana:/var/lib/grafana -e GF_SECURITY_ADMIN_USER=admin -e GF_SECURITY_ADMIN_PASSWORD=admin grafana/grafana:11.1.0
```

### Grafana Loki

В проекте используется в качестве централизованного хранилища логов.

Запуск в Docker:

```shell
docker run --name grafana-loki -p 3100:3100 grafana/loki:3.0.0
```